/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.Rockola.modelos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
/**
 *
 * @author Allison
 */
@Component("Usuarios")
//aaaaaaaaaaaaaah
public class Usuarios{
    @Autowired
    transient JdbcTemplate jdbcTemplate; // paquete que nos permite ejecutar la consulta sql
    // puede que no siempre se ejecute la consulta
    // atributos
    private int id_usuarios;
    private String nombre_usuario;
    private String apellido_usuarios;
    private float contrasena;
    private String correo;
    private int Id_playlist; //Llave foranea
    // constructor parametros (obliga a ingresar los datos solicitados)
    public Usuarios(String nombre_usuario, String apellido_usuarios, float contraseña, String correo, int id_usuario) {
        this.nombre_usuario = nombre_usuario;
        this.apellido_usuarios = apellido_usuarios;
        this.contrasena = contrasena;
        this.correo = correo;
        this.id_usuarios = id_usuario;
    }
    //metodos get and setters 

    public int getId_usuarios() {
        return id_usuarios;
    }

    public void setId_usuarios(int id_usuario) {
        this.id_usuarios = id_usuario;
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    public String getApellido_usuarios() {
        return apellido_usuarios;
    }

    public void setApellido_usuarios(String apellido_usuarios) {
        this.apellido_usuarios = apellido_usuarios;
    }

    public float getContrasena() {
        return contrasena;
    }

    public void setContrasena(float contrasena) {
        this.contrasena = contrasena;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Usuarios(int Id_playlist) {
        this.Id_playlist = Id_playlist;
    }

    @Override
    public String toString() {
        return "Usuarios{" + "id_usuarios=" + id_usuarios + ", nombre_usuario=" + nombre_usuario + ", apellido_usuarios=" + apellido_usuarios + ", contrasena=" + contrasena + ", correo=" + correo + ", Id_playlist=" + Id_playlist + '}';
    }
    
    
    // CRUD - C
    // Guardar
    public String guardar()throws ClassNotFoundException, SQLException{
        String sql = "INSERTE INTO usuarios(id_usuarios, nombre_usuario, apellido_usuario, contrasena, correo, Id_playlist) VALUES(?,?,?,?,?)";
        return sql;
    }
    //CRUD - R
    // Consultar
    public boolean consultar()throws ClassNotFoundException, SQLException{
        String sql = "SELECT id_usuarios, nombre_usuario, apellido_usuario, contrasena, correo, Id_playlist FROM usuarios WHERE id_usuarios = ?";
        // rs = apuntador
        List<Usuarios> usuario = jdbcTemplate.query(sql, (rs, rowNom)
              -> new Usuarios(
                      rs.getString("nombre_usuario"),
                      rs.getString("apellido_usuario"),
                      rs.getFloat("contrasena"),
                      rs.getString("correo"),
                      rs.getInt("id_usuario")
              ), new Object[]{this.getId_usuarios()});
        if (usuario != null &&  !usuario.isEmpty() ) {
            this.setId_usuarios(usuario.get(0).getId_usuarios());
            this.setNombre_usuario(usuario.get(0).getNombre_usuario());
            this.setApellido_usuarios(usuario.get(0).getApellido_usuarios());
            this.setContrasena(usuario.get(0).getContrasena());
            this.setCorreo(usuario.get(0).getCorreo());
            
            return true;
        }
        return false;
    }
    public List<Usuarios> consultarplaylist(int Id_playlist) throws ClassNotFoundException, SQLException{
        String sql = "SELECT id_usuarios, nombre_usuario, apellido_usuario, contrasena, correo FROM usuarios WHERE id_usuarios = ?";
        List<Usuarios> usuario = jdbcTemplate.query(sql, (rs, rowNom)
              -> new Usuarios(
                      rs.getString("nombre_usuario"),
                      rs.getString("apellido_usuario"),
                      rs.getFloat("contrasena"),
                      rs.getString("correo"),
                      rs.getInt("id_usuario")                     
              ), new Object[]{ Id_playlist });
        return usuario;
    }
    //CRUD - U
    // Actualizar
    public String actualizar()throws ClassNotFoundException, SQLException{
        String sql = "UPDATE usuario SET nombre_usuario =?, apellido_usuario =?, contrasena =?, correo =?, rol =? WHERE id_usuarios =?";
        return sql;
    }
    //CRUD - D
    //Eliminar
    public boolean borrar() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM usuario WHERE id_usuarios =?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getId_usuarios());
        ps.execute();
        ps.close();
        return true;
        
    }

}