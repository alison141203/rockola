/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.Rockola.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * */
@Component("playlist")
public class Playlist {
    @Autowired
    transient JdbcTemplate jdbcTemplate;
    //Atributos
    
    private int Id_playlist;//Llave Primaria
    private int Id_canciones;//Llave Foranea
    private int Id_generos;//Llave Foranea
    private int Id_artistas;//Llave Foranea
   
    //Constructor
//contrusctor con parametros
    public Playlist(int Id_playlist) { 
        this.Id_playlist = Id_playlist;
    }

//constructor Vacio
    public Playlist() {
    }
   

    public int getId_playlist() {    
        return Id_playlist;
    }

    public void setId_playlist(int Id_playlist) {
        this.Id_playlist = Id_playlist;
    }

    public int getId_canciones() {
        return Id_canciones;
    }

    //Metodos
    public void setId_canciones(int Id_canciones) {   
        this.Id_canciones = Id_canciones;
    }

    public int getId_generos() {
        return Id_generos;
    }

    public void setId_generos(int Id_generos) {
        this.Id_generos = Id_generos;
    }

    public int getId_artistas() {
        return Id_artistas;
    }

    public void setId_artistas(int Id_artistas) {
        this.Id_artistas = Id_artistas;
    }

    @Override
    public String toString() {
        return "Playlist{" + "Id_playlist=" + Id_playlist + ", Id_canciones=" + Id_canciones + ", Id_generos=" + Id_generos + ", Id_artistas=" + Id_artistas + '}';
    }
//CRUD
   
/*R*/
    //-Consultar------------------------------------------------------------------------------------------------------------------------
    public boolean consultarplaylist() throws ClassNotFoundException, SQLException {
        String sql = "SELECT * FROM playlist WHERE Id_playlist =?";
        List<Playlist> playlist = jdbcTemplate.query(sql, (rs, rowNom)//rowNom = nombre de las filas / rs = apuntador
                -> new Playlist(
                        rs.getInt("Id_playlist")
                        
                        ),new Object[]{this.getId_playlist()});
          
    if (playlist != null && !playlist.isEmpty()){
    this.setId_playlist(playlist.get(0).getId_playlist());
    return true;
    }
    else {
        
    }
        return false;
    }        
        public List<Playlist>consultarcanciones(int Id_canciones) throws ClassNotFoundException, SQLException {
            
          String sql = "SELECT * FROM playlist WHERE Id_playlist =?";
          
        List<Playlist> playlist = jdbcTemplate.query(sql, (rs, rowNom)//rowNom = nombre de las filas / rs = apuntador
                -> new Playlist(
                        rs.getInt("Id_playlist")
                        ),new Object[]{this.getId_canciones()});
        return playlist;
        
    } 
//------------------------------------------------------------------------------------------------------------------------------------
    
/*U*/
    //-Actualizar-------------------------------------------------------------------------------------------------------------------

        public String actualizar() throws ClassNotFoundException, SQLException {
        String sql = "UPDATE playlist SET Id_canciones=?, Id_usuarios=? WHERE Id_playlist =?";
        return sql;
        }
//-----------------------------------------------------------------------------------------------------------------------------------

/*D*/        
    //-Borrar------------------------------------------------------------------------------------------------------------------------

    public boolean borrar() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM playlist WHERE id =?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getId_playlist());
        ps.execute();
        ps.close();
        return true;    }
//prueba
//-------------------------------------------------------------------------------------------------------------------------------------
}
