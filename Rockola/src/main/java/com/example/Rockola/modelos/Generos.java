/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.Rockola.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Fransuat
 */
@Component("genero")
public class Generos {
    @Autowired
    transient JdbcTemplate jdbcTemplate;
    private int Id_generos;
    private String nombre_genero;
    private String tipo_genero;
    private String pais;
    private String categorias;

    public Generos(int Id_generos, String nombre_genero, String tipo_genero, String pais, String categorias) {
        this.Id_generos = Id_generos;
        this.nombre_genero = nombre_genero;
        this.tipo_genero = tipo_genero;
        this.pais = pais;
        this.categorias = categorias;
    }

    public Generos() {
    }

    public int getId_generos() {
        return Id_generos;
    }

    public void setId_generos(int Id_generos) {
        this.Id_generos = Id_generos;
    }

    public String getNombre_genero() {
        return nombre_genero;
    }

    public void setNombre_genero(String nombre_genero) {
        this.nombre_genero = nombre_genero;
    }

    public String getTipo_genero() {
        return tipo_genero;
    }

    public void setTipo_genero(String tipo_genero) {
        this.tipo_genero = tipo_genero;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCategorias() {
        return categorias;
    }

    public void setCategorias(String categorias) {
        this.categorias = categorias;
    }

    @Override
    public String toString() {
        return "Generos{" + "Id_generos=" + Id_generos + ", nombre_genero=" + nombre_genero + ", tipo_genero=" + tipo_genero + ", pais=" + pais + ", categorias=" + categorias + '}';
    }
public String guardar(){
    String sql = "INSERT INTO generos(Id_generos,nombre_genero,tipo_genero,pais,categorias)VALUES(?,?,?,?,?)";
    return sql;
}
public boolean consultar()throws ClassNotFoundException, SQLException{
    String sql = "SELECT Id_generos,nombre_genero,tipo_genero,pais,categorias FROM generos WHERE Id_generos = ?";
    List<Generos>generos =jdbcTemplate.query(sql, (rs, rowNom)
            ->new Generos(
            rs.getInt("Id_generos"),
                    rs.getString("nombre_genero"),
                    rs.getString("tipo_genero"),
                    rs.getString("pais"),
                    rs.getString("categorias")
            ),new Object[]{this.Id_generos()});
    if(generos !=null && !generos.isEmpty()){
        this.setId_generos(generos.get(0).getId_generos());
        this.setNombre_genero(generos.get(0).getNombre_genero());
        this.setTipo_genero(generos.get(0).getTipo_genero());
        this.setPais(generos.get(0).getPais());
        this.setCategorias(generos.get(0).getCategorias());
        
        return true;
    }
    return false;   
}
//CRUD - U
    // Actualizar
public String actualizar()throws ClassNotFoundException, SQLException{
    String sql = "UPDATE generos SET nombre_genero =?, tipo_genero =?, pais =?, categorias =? WHERE Id_generos =?";
    return sql;
}
//CRUD - D
    //Eliminar
public boolean borrar() throws ClassNotFoundException, SQLException{
    String sql ="DELETE FROM generos WHERE Id_generos =?";
    Connection c =jdbcTemplate.getDataSource().getConnection();
    PreparedStatement ps = c.prepareStatement(sql);
    ps.setInt(1,this.getId_generos());
    ps.execute();
    ps.close();
    return true;
}
    private Object Id_generos() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
  
   