/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.Rockola.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Andy Oviedo
 */

@Component("canciones")
public class Canciones{

    @Autowired
    transient JdbcTemplate jdbcTemplate;//Paquete que nos permite ejecutar  las consultas sql
    //Puede que no siempre se ejecute la consulta
 
    private int Id_canciones;
    private String nombre_canciones;
    private Artistas Id_artista;//LLave foránea de tabla artistas
    private Generos Id_generos;//Llave foránea de tabla generos
    private int anio;

    public Canciones(int Id_canciones, String nombre_canciones, int anio) {
        super();//Hereda de la clase principal
        this.Id_canciones = Id_canciones;
        this.nombre_canciones = nombre_canciones;
        this.anio = anio;
    }

    public Canciones() {
    }

    public int getId_canciones() {
        return Id_canciones;
    }

    public void setId_canciones(int Id_canciones) {
        this.Id_canciones = Id_canciones;
    }

    public String getNombre_canciones() {
        return nombre_canciones;
    }

    public void setNombre_canciones(String nombre_canciones) {
        this.nombre_canciones = nombre_canciones;
    }

    public Artistas getId_artista() {
        return Id_artista;
    }

    public void setId_artista(Artistas id_artista) {
        this.Id_artista = id_artista;
    }

    public Generos getId_generos() {
        return Id_generos;
    }

    public void setId_generos(Generos Id_generos) {
        this.Id_generos = Id_generos;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    @Override
    public String toString() {
        return "Canciones{" + "Id_canciones=" + Id_canciones + ", nombre_canciones=" + nombre_canciones + ", Id_artista=" + Id_artista + ", Id_generos=" + Id_generos + ", anio=" + anio + '}';
    }
    
    

    //CRUD -CREATE (C)
    //GUARDAR
    public String guardar() {
        String sql = "INSERT INTO canciones(Id_canciones,nombre_canciones,Id_artista, Id_generos,anio)VALUES(?,?,?,?,?)";
        return sql;
    }

    //CRUD -READ
    //CONSULTAR
    public boolean consultar() throws ClassNotFoundException, SQLException {
        String sql = "SELECT Id_canciones, nombre_canciones, Id_artista, Id_generos, anio FROM Canciones WHERE Id_canciones = ? ";
        List<Canciones> canciones = jdbcTemplate.query(sql, (rs, rowNom)//rowNom = nombre de las filas / rs = apuntador
                -> new Canciones(
                        rs.getInt("Id_canciones"),
                        rs.getString("nombre_canciones"),
                        rs.getInt("anio")
                ), new Object[]{this.getId_generos()});
        if (canciones != null && !canciones.isEmpty()) {
            this.setId_canciones(canciones.get(0).getId_canciones());
            this.setNombre_canciones(canciones.get(0).getNombre_canciones());
            this.setAnio(canciones.get(0).getAnio());

            return true;
        } else {
            return false;
        }
    }    
        

    public List<Canciones> consultarTodo(int Id_generos) throws ClassNotFoundException, SQLException {
        String sql = "SELECT Id_canciones,nombre_canciones,anio FROM canciones WHERE Id_canciones = ? ";
        List<Canciones> canciones = jdbcTemplate.query(sql, (rs, rowNom)
                -> new Canciones(
                        rs.getInt("Id_canciones"),
                        rs.getString("nombre_canciones"),
                        rs.getInt("anio")
                ), new Object[]{ Id_generos });
        return canciones;
    }
    //CRUD - U
    // Actualizar

    public String actualizar() throws ClassNotFoundException, SQLException {
        String sql = "UPDATE canciones SET nombre_canciones =?, Id_artista =?, Id_generos =?, anio =? WHERE Id_canciones =?";
        return sql;
    }
    //CRUD - D
    //Eliminar
    public boolean borrar() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM usuario WHERE id =?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getId_canciones());
        ps.execute();
        ps.close();
        return true;
        
    }
}


