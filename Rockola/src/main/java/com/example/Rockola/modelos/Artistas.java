/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.Rockola.modelos;

import java.io.Serializable;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
/**
 *
 * @author FAMILIA MORENO
 */
@Component("artistas")
public class Artistas implements Serializable{
    @Autowired
    transient JdbcTemplate jdbcTemplate; // paquete que nos permite ejecutar la consulta sql
    // puede que no siempre se ejecute la consulta
    // atributos
    
    //ATRIBUTOS
    
    private int id_artista ;
    private String nombre_artista;
    private String pais_artista;
    private int id_genero;
    
    
    //CONSTRUCTOR

    public Artistas(int id_artista, String nombre_artista, String pais_artista) {
        this.id_artista = id_artista;
        this.nombre_artista = nombre_artista;
        this.pais_artista = pais_artista;
       
    }
    
    //CONSTRUCTOR VACIO

    public Artistas() {
    }
    
    
    // GETTERS && SETTERS

    public int getId_artista() {
        return id_artista;
    }

    public void setId_artista(int id_artista) {
        this.id_artista = id_artista;
    }

    public String getNombre_artista() {
        return nombre_artista;
    }

    public void setNombre_artista(String nombre_artista) {
        this.nombre_artista = nombre_artista;
    }

    public String getPais_artista() {
        return pais_artista;
    }

    public void setPais_artista(String pais_artista) {
        this.pais_artista = pais_artista;
    }

    public int getId_genero() {
        return id_genero;
    }

    public void setId_genero(int id_generos) {
        this.id_genero = id_generos;
    }

    @Override
    public String toString() {
        return "Artistas{" + "id_artista=" + id_artista + ", nombre_artista=" + nombre_artista + ", pais_artista=" + pais_artista + ", id_generos=" + id_genero + '}';
    }
    
    // CRUD - C
    // Guardar
    /*public String guardar()throws ClassNotFoundException, SQLException{
        String sql = "INSERTE INTO Artistas(id_artista, nombre_artista,pais_artista) VALUES(?,?,?)";
        return sql;
    }*/
    
    //CRUD - R
    // Consultar
    public boolean consultar() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        String  sql = "SELECT id_artista ,nombre_artista ,pais_artista,id_generos FROM cuentas WHERE id_generos = ?";
        List<Artistas> artistas = jdbcTemplate.query(sql, (rs, rowNum)
                 -> new Artistas(
                        rs.getInt("id_artistas"),
                        rs.getString("nombre"),
                        rs.getString("pais")
                           ),new Object[]{this.getId_genero()});
        if (artistas != null &&  !artistas.isEmpty() ) {
            this.setId_artista(artistas.get(0).getId_artista());
            this.setNombre_artista(artistas.get(0).getNombre_artista());
            this.setPais_artista(artistas.get(0).getPais_artista());
            
            return true;
        }
        else {
            return false;
        }
    }
    
     public List<Artistas> consultarTodo(int id_generos)throws SQLException{
        String sql = "SELECT id_artistas,nombre_artista,pais_artista FROM Artistas WHERE id_generos = ? ";
        List<Artistas> artistas = jdbcTemplate.query(sql,(rs,rowNom)
                -> new Artistas(
                        rs.getInt("id_artistas"),
                        rs.getString("nombre"),
                        rs.getString("pais")
                           ),new Object[]{this.getId_genero()});
         return artistas;
    }


    //CRUD - U
    // Actualizar
    public String actualizar()throws ClassNotFoundException, SQLException{
        String sql = "UPDATE Artistas SET nombre_artista =?, pais_artista =?,  WHERE id_artista =?";
        return sql;
    }
    //CRUD - D
    //Eliminar
    /*public boolean borrar() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM Artistas WHERE id_artistas =?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getId_artista());
        ps.execute();
        ps.close();
        return true;
        
    }*/
}
