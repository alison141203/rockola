package com.example.Rockola;

import com.google.gson.Gson;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.example.Rockola.modelos.Canciones;
import com.example.Rockola.modelos.Generos;

@SpringBootApplication //Indica que es la clase Main
@RestController //Indica que la clase será API REST
public class RockolaApplication {

    @Autowired(required = true)
    Canciones cancion;
    @Autowired(required = true)
    Generos gen;

    public static void main(String[] args) {
        SpringApplication.run(RockolaApplication.class, args);
    }

    @GetMapping("/canciones")
    public String consultarCancionPorId(@RequestParam(value = "id", defaultValue = "1") int id) throws ClassNotFoundException, SQLException {
        cancion.setId_canciones(id);
        if (cancion.consultar()) {
            String res = new Gson().toJson(cancion);
            cancion.setId_canciones(0);
            cancion.setNombre_canciones("");
            cancion.setAnio(0);
            return res;
        } else {
            return new Gson().toJson(cancion);
        }
    }
    
    @GetMapping("/generos")
    public String consultarGeneroPorId(@RequestParam(value = "id", defaultValue = "1") int id) throws ClassNotFoundException, SQLException {
        gen.setId_generos(id);
        if (gen.consultar()) {
            String res = new Gson().toJson(gen);
            gen.setNombre_genero("");
            gen.setTipo_genero("");
            gen.setPais("");
            gen.setCategorias(res);
            
            return res;
        } else {
            return new Gson().toJson(gen);
        }
    }

    @GetMapping("/canciones")
    public String consultarCancionesPorGenero(@RequestParam(value = "id", defaultValue = "1") int id) throws ClassNotFoundException, SQLException {
        List<Canciones> canciones = cancion.consultarTodo(id);
        if (canciones.size() > 0) {
            return new Gson().toJson(canciones);
            
        } else {
            return new Gson().toJson(canciones);
            
        }
    }

    @GetMapping("/generocancion")
    public String consultarCancionPorGenero(@RequestParam(value = "id", defaultValue = "1") int id) throws ClassNotFoundException, SQLException {
        gen.setId_generos(id);
        if (gen.consultar()) {
            return new Gson().toJson(gen);
        } else {
            return new Gson().toJson(gen);
        }
    }

    @PostMapping(path = "/generos",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    
    public String actualizarCancion(@RequestBody String canciones) throws ClassNotFoundException, SQLException {
        Canciones c = new Gson().fromJson(canciones, Canciones.class);
        cancion.setId_canciones(c.getId_canciones());
        cancion.setNombre_canciones(c.getNombre_canciones());
        cancion.setAnio(c.getAnio());
        cancion.actualizar();
        return new Gson().toJson(c);
    }
    @DeleteMapping("/eliminarcancion/{id}")
    public String borrarCancion(@PathVariable("id")int id) throws ClassNotFoundException, SQLException {
        cancion.setId_canciones(id);
        cancion.borrar();
        return "Los datos del id indicado han sido eliminados";
    }
}
