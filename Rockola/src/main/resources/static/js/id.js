angular.module('rockola',[])
.controller('filasCanciones',function($scope,$http){
    
    $scope.nombre_genero = "";
    $scope.tipo_genero = "";
    $scope.pais = "";
    $scope.categorias = "";
    
    // accion del boton consultar
    $scope.consultar = function(){
        if($scope.id === undefined || $scope.id === null){
            $scope.id = 0;
        }
        //Consulta si existe y muestra
        $http.get("/rockola/generoid?id="+$scope.id).then(function(data){
            console.log(data.data);
            $scope.nombre_genero = data.data.nombre_genero;
            $scope.tipo_genero = data.data.tipo_genero;
            $scope.pais = data.data.pais;
            $scope.categorias = data.data.categorias;
        },function(){
             //error
            $scope.nombre_genero = "";
            $scope.tipo_genero = "";
            $scope.pais = "";
            $scope.categorias = "";
            $scope.filas = []; // guarda
        });
        //Se termina la consulta y se muestra
        //Llena lista y poner datos en la tabla html
        $http.get("/rockola/cancion?id="+$scope.id).then(function(data){
            console.log(data.data);
            $scope.filas = data.data;  
        },function(){
            $scope.filas = [];
        });
    };
    
    //acción del botón actualizar
    $scope.actualizar = function(){
        if($scope.id === undefined || $scope.id === null){
            $scope.id = 0;
        }
        data = {
            "id": $scope.id,
            "nombre_genero":$scope.nombre_genero,
            "tipo_genero": $scope.tipo_genero,
            "pais": $scope.pais,
            "categorias": $scope.categorias
        };
        $http.post('/rockola/generos', data).then(function(data){
            //success
            $scope.nombre_genero = data.data.nombre_genero;
            $scope.tipo_genero = data.data.tipo_genero;
            $scope.pais = data.data.pais;
            $scope.categorias = data.data.categorias;
        },function(){
            //error
            $scope.nombre_genero = "";
            $scope.tipo_genero = "";
            $scope.pais = "";
            $scope.categorias = "";
            $scope.filas = [];
        });
        
    };
    
    //guardar registro
    $scope.guardar = function(){
        data = {
            "nombre_genero":$scope.nombre_genero,
            "tipo_genero": $scope.tipo_genero,
            "pais": $scope.pais,
            "categorias": $scope.categorias
        };
        $http.post('/rockola/genero', data).then(function(data){
            //success
            $scope.nombre_genero = data.data.nombre_genero;
            $scope.tipo_genero = data.data.tipo_genero;
            $scope.pais = data.data.pais;
            $scope.categorias = data.data.categorias;
            alert();
        },function(){
            //error
            $scope.nombre_genero = "";
            $scope.tipo_genero = "";
            $scope.pais = "";
            $scope.categorias = "";
            $scope.filas = [];
        });
        
    };
    
    
    // acción del botón borrar
    $scope.borrar = function(){
        if($scope.id === undefined || $scope.id === null){
            $scope.id = 0;
        }
        data = {
            "id": $scope.id
        };
        $http.delete('/rockola/eliminarcancion/'+$scope.idcuenta, data).then(function(data){
            alert("La cuenta ha sido eliminada");
            
        },function(){
            alert("Ha ocurrido un error");
        });
    };
});


